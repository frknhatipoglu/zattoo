//
//  zattooTests.swift
//  zattooTests
//
//  Created by Furkan Hatipoglu on 16.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import XCTest
@testable import zattoo
import Alamofire

class zattooTests: XCTestCase {

    //    var service  = ServiceFunctions()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testUuidGenerator () {
        XCTAssertNotNil(UUID(uuidString: Helpers.shared.getUuidString()), "uuid generator is not valid")
    }

    func testSessionHello () {
        //        test session hello service
        var sessionHelloResponseIsSuccess = false
        let sessionHelloExpectation = expectation(description: "testSessionHello")

        var sessionHelloParameters = [String: Any]()
        sessionHelloParameters["app_tid"] = GlobalVariables.appId //get app id
        sessionHelloParameters["uuid"] = Helpers.shared.getUuidString()  //get uuid
        sessionHelloParameters["lang"] = "en"
        sessionHelloParameters["format"] = "json"


        ServiceFunctions().postSessionHello(parameters: sessionHelloParameters) { (responseData) in
            sessionHelloResponseIsSuccess = DataParser.shared.parseServiceDataForSuccess(responseData: responseData).success
            sessionHelloExpectation.fulfill()
        }

        waitForExpectations(timeout: 5.0, handler: nil)
        print(sessionHelloResponseIsSuccess)
        XCTAssertTrue(sessionHelloResponseIsSuccess, "Session start response is false")

        //        test login service
        var loginResponseIsSuccess = false
        let loginExpectation = expectation(description: "testLoginService")

        var loginParameters = [String: Any]()
        loginParameters["login"] = GlobalVariables.loginValue
        loginParameters["password"] = GlobalVariables.passwordValue

        ServiceFunctions().postLogin(parameters: loginParameters) { (responseData) in
            loginResponseIsSuccess = DataParser.shared.parseServiceDataForSuccess(responseData: responseData).success
            loginExpectation.fulfill()
        }

        waitForExpectations(timeout: 5.0, handler: nil)
        print(loginResponseIsSuccess)
        XCTAssertTrue(loginResponseIsSuccess, "Login servise response is false")

        //        test watch service
        var watchResponseIsSuccess = false
        var streamUrl = ""
        let watchExpectation = expectation(description: "testLoginService")

        var watchParameters = [String: Any]()
        watchParameters["cid"] = "dsf"
        watchParameters["stream_type"] = "hls"

        ServiceFunctions().postWatch(parameters: watchParameters) { (responseData) in
            let parsedData = DataParser.shared.parseWatchData(responseData: responseData)
            watchResponseIsSuccess = parsedData.success
            streamUrl = parsedData.streamData?.streamUrl ?? ""
            watchExpectation.fulfill()
        }

        waitForExpectations(timeout: 5.0, handler: nil)
        print(watchResponseIsSuccess)
        XCTAssertTrue(watchResponseIsSuccess, "Watch servise response is false")
        XCTAssertNotEqual(streamUrl, "", "Stream url is empty")


        //        test stream url
        var streamUrlStatusCode: Int?
        let streamUrlExpectation = expectation(description: "streamUrl")

        Alamofire.request(streamUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).response { (response) in
            streamUrlStatusCode = response.response?.statusCode
            streamUrlExpectation.fulfill()

        }
        waitForExpectations(timeout: 5.0, handler: nil)
        XCTAssertEqual(streamUrlStatusCode, 200)
    }

    func testDataParser () {
        let path = Bundle.main.path(forResource: "watchResponse", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let parsedData = DataParser.shared.parseServiceDataForSuccess(responseData: data)

        XCTAssertTrue(parsedData.success, "Watch servise response is false")
        XCTAssertNotEqual(parsedData.streamData?.streamUrl, "", "Stream url is empty")

    }
}
