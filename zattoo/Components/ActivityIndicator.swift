//
//  ActivityIndicator.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 16.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import UIKit

class ActivityIndicator: UIActivityIndicatorView {

    override init(frame: CGRect) {

        let newFrame = CGRect(x: frame.midX - 15, y: frame.midY - 15, width: 30, height: 30)

        super.init(frame: newFrame)

        self.isUserInteractionEnabled = false
        self.color = UIColor.white
        self.style = .whiteLarge
        self.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        self.hidesWhenStopped = true
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
