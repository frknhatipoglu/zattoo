//
//  Colors.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 20.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import UIKit

extension UIColor {
    struct customColors {
        static let orangeColor = UIColor(red: 240/255, green: 126/255, blue: 49/255, alpha: 1)
    }
}
