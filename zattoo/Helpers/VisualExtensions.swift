//
//  VisualExtensions.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 20.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import UIKit

extension UIView {

    func hideWithAnimation () {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        })
    }

    func showWithAnimation () {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        })
    }
}
