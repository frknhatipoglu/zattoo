//
//  Helpers.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 20.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import Foundation

class Helpers {

    static let shared = Helpers()

    private init () {

    }

    func getUuidString () -> String {
        return UUID().uuidString
    }

}
