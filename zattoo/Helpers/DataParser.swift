//
//  DataParser.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 18.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import Foundation
import SwiftyJSON

class DataParser {

    static let shared = DataParser()

    private init () {

    }

    func parseServiceDataForSuccess (responseData: Data) -> serviceResponseDataStruct {

        let responseJson = JSON(responseData)
        var serviceResponseData = serviceResponseDataStruct()
        serviceResponseData.success = responseJson["success"].boolValue
        return serviceResponseData
    }

    func parseWatchData (responseData: Data) -> serviceResponseDataStruct {

        let responseJson = JSON(responseData)
        var serviceResponseData = serviceResponseDataStruct()
        serviceResponseData.success = responseJson["success"].boolValue
        serviceResponseData.streamData = streamDataStruct()
        serviceResponseData.streamData?.streamUrl = responseJson["stream"]["url"].stringValue
        return serviceResponseData
    }


}
