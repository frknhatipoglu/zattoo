//
//  DataModels.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 20.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import Foundation

struct serviceResponseDataStruct {
    var success: Bool = false
    var streamData: streamDataStruct?
}

struct streamDataStruct {
    var streamUrl: String?
}
