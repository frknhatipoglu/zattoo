//
//  CustomPlayerControllerViewController.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 20.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import UIKit
import AVKit

class CustomPlayerController: AVPlayerViewController {

    var viewDidDisappearCompletionHandler: (() -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidDisappear(_ animated: Bool) {
        viewDidDisappearCompletionHandler?()
    }

}
