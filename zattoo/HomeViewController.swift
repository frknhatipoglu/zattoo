//
//  HomeViewController.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 17.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import UIKit
import AVKit

class HomeViewController: BaseViewController {

    private let playerViewController = CustomPlayerController()
    let playButton = UIButton(type: .system)

//  change status bar color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setComponents()
        setNotifications()
    }

    func setComponents () {
        setPlayButton()
        setPlayerControllerProperties()
    }

//    create a play button to present player controller again
    func setPlayButton () {
        playButton.frame = CGRect(x: pageScrollView.bounds.midX - 80, y: pageScrollView.bounds.midY - 40, width: 160, height: 40)
        playButton.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        playButton.backgroundColor = UIColor.customColors.orangeColor
        playButton.setTitle("Play", for: .normal)
        playButton.tintColor = UIColor.white
        playButton.addTarget(self, action: #selector(HomeViewController.playButtonPressed(_:)), for: .touchUpInside)
        playButton.hideWithAnimation()
        pageScrollView.addSubview(playButton)
    }

    @objc func playButtonPressed (_ sender: UIButton) {
        getDataAndPlayStream()
    }

//    get necessery datas to present player view controller
    func getDataAndPlayStream () {
        playButton.hideWithAnimation()
        getStartSessionData()
    }

//     set up app app lifecycle notifications
    func setNotifications () {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomeViewController.applicationStateForegroundOrLaunched(_ :)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomeViewController.applicationStateForegroundOrLaunched(_ :)),
                                               name: UIApplication.didFinishLaunchingNotification,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomeViewController.applicationStateBackgroundOrTerminted(_ :)),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomeViewController.applicationStateBackgroundOrTerminted(_ :)),
                                               name: UIApplication.willTerminateNotification,
                                               object: nil)
    }

    @objc func applicationStateForegroundOrLaunched (_ sender: NSNotification) {
        print("applicationStateForegroundOrLaunched")
        getDataAndPlayStream()
    }

    @objc func applicationStateBackgroundOrTerminted (_ sender: NSNotification) {
        print("applicationStateBackgroundOrTerminted")
        playerViewController.dismiss(animated: true, completion: nil)
    }

    func callPlayerCloseServices () {
//        we don't have to wait for response data,so funtions are called async
        ServiceFunctions().postSessionGoodbye { (responseData) in
            let parsedData = DataParser.shared.parseServiceDataForSuccess(responseData: responseData)
            print("session goodbye service response: \(parsedData.success)")
        }

        ServiceFunctions().getStop { (responseData) in
            let parsedData = DataParser.shared.parseServiceDataForSuccess(responseData: responseData)
            print("Get stop service response: \(parsedData.success)")
        }
    }

//    first call to start session
    func getStartSessionData () {
        var paramters = [String: Any]()
        paramters["app_tid"] = GlobalVariables.appId //get app id
        paramters["uuid"] = Helpers.shared.getUuidString()  //get uuid
        paramters["lang"] = "en"
        paramters["format"] = "json"

        self.activityIndicator?.startAnimating()
        ServiceFunctions().postSessionHello(parameters: paramters) { (responseData) in
            self.activityIndicator?.stopAnimating()
            self.parseStartSessionData(responseData: responseData)
        }
    }

    func parseStartSessionData (responseData: Data) {
        let parsedData = DataParser.shared.parseServiceDataForSuccess(responseData: responseData)
        parsedData.success ? getLoginData() : generateErrorWith(message: "An error occured while processing session start data")
        print("start session service response: \(parsedData.success)")
    }

//  second call to login, wait response of start session
    func getLoginData () {
        var paramters = [String: Any]()
        paramters["login"] = GlobalVariables.loginValue
        paramters["password"] = GlobalVariables.passwordValue

        self.activityIndicator?.startAnimating()
        ServiceFunctions().postLogin(parameters: paramters) { (responseData) in
            self.activityIndicator?.stopAnimating()
            self.parseLoginData(responseData: responseData)
        }
    }

    func parseLoginData (responseData: Data) {
        let parsedData = DataParser.shared.parseServiceDataForSuccess(responseData: responseData)
        parsedData.success ? getWatchData() : generateErrorWith(message: "An error occured while processing login data")
        print("login service response: \(parsedData.success)")
    }

//    third call to get stream url, wait response of login service
    func getWatchData () {
        var paramters = [String: Any]()
        paramters["cid"] = "dsf"
        paramters["stream_type"] = "hls"

        self.activityIndicator?.startAnimating()
        ServiceFunctions().postWatch(parameters: paramters) { (responseData) in
            self.activityIndicator?.stopAnimating()
            self.parseWatchData(responseData: responseData)
        }
    }

    func parseWatchData (responseData: Data) {
        let parsedData = DataParser.shared.parseWatchData(responseData: responseData)
        print("watch service response: \(parsedData.success)")

        if !parsedData.success {
            generateErrorWith(message: "An error occured while stream url data")
            return
        }

        guard let streamString = parsedData.streamData?.streamUrl else {
            generateErrorWith(message: "An error occured while stream url data.")
            return
        }
        guard let streamUrl = URL(string: streamString) else {
            generateErrorWith(message: "An error occured while stream url data. Stream url is not valid url.")
            return
        }

        print("stream url: \(streamUrl)")

        playerViewController.player = AVPlayer(url: streamUrl)
        self.present(playerViewController, animated: true) {
            self.playerViewController.player?.play()
        }
    }

//    an error occured with services, show play button and create and alert view to user
    func generateErrorWith (title: String = "Error", message: String) {
        playButton.showWithAnimation()
        createAlertView(title: title, message: message)
    }

    func setPlayerControllerProperties () {
//        set an completion handler for player controller didDisappear
        playerViewController.viewDidDisappearCompletionHandler = playerViewControllerDidDisappearCompletionHandler
    }

//  player controller is dismissed, show play button and call necessery services
    func playerViewControllerDidDisappearCompletionHandler () {
        playButton.showWithAnimation()
        callPlayerCloseServices()
    }

}
