//
//  PageViewController.swift
//
//  Copyright © 2016 Furkan Hatipoglu. All rights reserved.
//

import UIKit

class PageViewController: UIViewController {

    let pageScrollView = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        pageScrollView.frame = self.view.bounds
        pageScrollView.showsHorizontalScrollIndicator = false
        pageScrollView.showsVerticalScrollIndicator = false
        pageScrollView.backgroundColor = UIColor.black
        pageScrollView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        view.addSubview(pageScrollView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
