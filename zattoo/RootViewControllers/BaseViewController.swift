
import UIKit

class BaseViewController: PageViewController {
    
    var activityIndicator: ActivityIndicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setActivityIndicator()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func setActivityIndicator (_ withFrame: CGRect? = nil) {
        if let frame = withFrame {
            activityIndicator = ActivityIndicator(frame: frame)
        } else {
            activityIndicator = ActivityIndicator(frame: pageScrollView.frame)
        }
        activityIndicator?.layer.zPosition = 1
        pageScrollView.addSubview(activityIndicator!)
    }

    func createAlertView (title: String, message: String) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

        if let popoverController = alertViewController.popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }

        alertViewController.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))

        self.present(alertViewController, animated: true, completion: nil)
    }
}
