//
//  ServiceFunctions.swift
//  zattoo
//
//  Created by Furkan Hatipoglu on 16.04.2019.
//  Copyright © 2019 furkanhatipoglu. All rights reserved.
//

import Alamofire

class ServiceFunctions {


    let apiRootUrl = "https://sandbox.zattoo.com/zapi/"


    init() {

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //  sample paramters["app_tid"] = GlobalVariables.appId
    //  paramters["uuid"] = Helpers.shared.getUuidString()
    //  paramters["lang"] = "en"
    //  paramters["format"] = "json"
    func postSessionHello (parameters: [String: Any], completionHandler: @escaping (_ data: Data) -> ()) {

        let url = apiRootUrl + "session/hello"

        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).response { (response) in
            if let data = response.data as Data? {
                completionHandler(data)
            }
        }
    }

//    expected paramters "login", "password"
    func postLogin (parameters: [String: Any], completionHandler: @escaping (_ data: Data) -> ()) {

        let url = apiRootUrl + "v2/account/login"

        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).response { (response) in
            if let data = response.data as Data? {
                completionHandler(data)
            }
        }
    }

    //  sample paramters["cid"] = "dsf" paramters["stream_type"] = "hls"
    func postWatch (parameters: [String: Any], completionHandler: @escaping (_ data: Data) -> ()) {

        let url = apiRootUrl + "watch"

        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).response { (response) in
            if let data = response.data as Data? {
                completionHandler(data)
            }
        }
    }

    func getStop (completionHandler: @escaping (_ data: Data) -> ()) {

        let url = apiRootUrl + "stop"

        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).response { (response) in
            if let data = response.data as Data? {
                completionHandler(data)
            }
        }
    }

    func postSessionGoodbye (completionHandler: @escaping (_ data: Data) -> ()) {

        let url = apiRootUrl + "session/goodbye"

        Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).response { (response) in
            if let data = response.data as Data? {
                completionHandler(data)
            }
        }
    }



}
