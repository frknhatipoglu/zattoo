# Zattoo App

This app requests to session hello, login and watch services and presents an AVPlayerController.

## Getting Started

### Prerequisites

- Swift 4.2
- Xcode 10.1

Don't forget the work on .xcworkspace file.

### Folder Structure
    - zattoo
        - AppDelegate
        - HomeViewController
        - CustomPlayerControllerViewController
        - RootViewControllers
            - BaseViewController
            - PageViewController
        - ServiceHelpers
            - ServiceFunctions
        - Helpers
            - GlobalVariables
            - Helpers
            - VisualExtensions
            - Colors
            - DataModels
            - DataParser
        - Components
            - ActivityIndicator
    - zattooTests
        - SampleResponses
            - watchResponse
        - zattooTests


## Built With

Pod list for third party libraries:

* [Alamofire](https://github.com/Alamofire/Alamofire) - used for network operations
* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) - used for JSON parsing


## Tests

The project include unit tests that test service functions, data parser functions and uuid generator.

